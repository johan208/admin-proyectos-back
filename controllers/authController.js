const Usuario = require('../models/Usuario')
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator')
const jwt = require('jsonwebtoken');

exports.autenticarUsuario = async (req, res) => {

    // extraer el email y pass
    const { email, password } = req.body;

    try {
        console.log(email)
        //revisar correo registrado
        let usuario = await Usuario.findOne({ email }) //{email:email} === {email}
        console.log(usuario)
        if(!usuario) {
            return res.status(400).json({msg: 'El usuario no existe'})
            
        }

        //revisar pass
        const passOk = await bcryptjs.compare(password, usuario.password) //usuario.password es el hasheado
        if(!passOk) {
            return res.status(400).json({msg: 'Password incorrecto'})
        }

            // Si todo ok crear y firmar JWT
            const payload = {
                usuario: {
                    id: usuario.id
                }
            };

            // Firmar token
            jwt.sign(payload, process.env.SECRETA, {
                expiresIn: 7200 // 2 horas
            }, (error, token) => {
                if(error) throw error;

                // Mensaje de confirmacion {token: token} === { token }
                res.json({ token });
            });

    } catch (error) {
        console.log(error);
    }
}

// Obtiene que usuario esta autenticado
exports.usuarioAutenticado = async (req, res) => {

    try {
        const usuario = await Usuario.findById(req.usuario.id).select('-password');
        res.json({ usuario })

    } catch(error) {
        console.log(error);
        res.status(500).send({ msg : 'Hubo un error'});
    }
}
