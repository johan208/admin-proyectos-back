const Usuario = require('../models/Usuario')
const bcryptjs = require('bcryptjs');
const { validationResult } = require('express-validator')
const jwt = require('jsonwebtoken');

exports.crearUsuario = async (req, res) => {

    // revisar si hay errores
    const errores = validationResult(req);
    if(!errores.isEmpty() ) {
        return res.status(400).json({ errores: errores.array() })
    }

    // extraer email y password
    const { email, password } = req.body;

    try {
        // validar que el usuario registrado sea unico
        let usuario = await Usuario.findOne({ email });
        if(usuario) {
            return res.status(400).json({ msg: "El usuario ya existe"})
        }

        // crea el nuevo usuario
        usuario = new Usuario(req.body);

        //hashear contraseña
        const salt = await bcryptjs.genSalt(10);
        usuario.password = await bcryptjs.hash( password, salt );

        // guarda el nuevo usuario
        await usuario.save();

        // Crear y firmar JWT
        const payload = {
            usuario: {
                id: usuario.id
            }
        };

        // Firmar token
        jwt.sign(payload, process.env.SECRETA, {
            expiresIn: 7200 // 2 horas
        }, (error, token) => {
            if(error) throw error;

            // Mensaje de confirmacion {token: token} === { token }
            res.json({ token });
        });

    } catch (error) {
        console.log(error);
        res.status(400).send('Hubo un error')
    }
} 