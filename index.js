const express = require('express');
const conectarDB = require('./config/db');
const cors = require('cors');

//crear servidor
const app = express();

//conectar BD
conectarDB();

// Habilitar cors

app.use(cors());

//Habilitar express.json (header/jason) que es remplazo a body parser
app.use(express.json({ extended: true }));

// puerto de la app server
const port = process.env.PORT || 4000;

// importar rutas
app.use('/api/usuarios', require('./routes/usuarios'));
app.use('/api/auth', require('./routes/auth'));
app.use('/api/proyectos', require('./routes/proyectos'));
app.use('/api/tareas', require('./routes/tareas'))


// arrancar app
app.listen(port, '0.0.0.0', () => {
    console.log(`Servidor ok en el puerto ${port}`);
})